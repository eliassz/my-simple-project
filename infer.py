import pandas as pd
from my_simple_project.utils import (
    evaluate_model,
    predict_with_loaded_model,
    prepare_data,
)

import config as cfg

if __name__ == "__main__":
    data = pd.read_csv(cfg.INFER_DATA_PATH)
    data_cleaned = prepare_data(data)
    X, y = data_cleaned.drop("Survived", axis=1), data_cleaned["Survived"]
    y_pred = predict_with_loaded_model(X, submission_path=cfg.SUBMISSION_PATH)
    accuracy = evaluate_model(y, y_pred)
    print(f"inference accuracy: {accuracy}")
