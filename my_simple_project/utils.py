import joblib
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split

import config as cfg

__all__ = [
    "prepare_data",
    "split_data",
    "train_model",
    "predict_with_model",
    "predict_with_loaded_model",
    "evaluate_model",
]


def prepare_data(df):
    """
    Подготовка данных для моделирования.
    """
    # Удаление менее релевантных признаков
    df_cleaned = df.drop(columns=["PassengerId", "Name", "Ticket", "Cabin"])

    # Заполнение пропущенных значений
    df_cleaned["Age"].fillna(df_cleaned["Age"].median(), inplace=True)
    df_cleaned["Embarked"].fillna(df_cleaned["Embarked"].mode()[0], inplace=True)

    # Преобразование категориальных признаков в числовые значения
    df_cleaned = pd.get_dummies(
        df_cleaned, columns=["Sex", "Embarked"], drop_first=True
    )

    return df_cleaned


def split_data(df_cleaned):
    """
    Разделение данных на обучающую и тестовую выборки.
    """
    X = df_cleaned.drop("Survived", axis=1)
    y = df_cleaned["Survived"]
    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=0.2, random_state=42
    )

    return X_train, X_test, y_train, y_test


def train_model(X_train, y_train, save=True, model_path=cfg.MODEL_PATH):
    """
    Обучение модели логистической регрессии.
    Если параметр save установлен в True, модель будет сохранена в файл.
    """
    model = LogisticRegression(max_iter=1000)
    model.fit(X_train, y_train)

    if save:
        joblib.dump(model, model_path)

    return model


def predict_with_model(model, X_test):
    """
    Предсказание с помощью обученной модели.
    """
    y_pred = model.predict(X_test)

    return y_pred


def predict_with_loaded_model(
    X_test, model_filename=cfg.MODEL_PATH, submission_path=None
):
    """
    Загрузка модели из файла и предсказание на тестовой выборке.
    """
    # Загрузка модели
    loaded_model = joblib.load(model_filename)

    # Предсказание
    y_pred = loaded_model.predict(X_test)

    if submission_path:
        submission = pd.DataFrame({"Survived": y_pred})
        submission.to_csv(submission_path, index=False)

    return y_pred


def evaluate_model(y_test, y_pred):
    """
    Оценка качества модели.
    """
    accuracy = accuracy_score(y_test, y_pred)

    return accuracy
