import pandas as pd
from my_simple_project.utils import (
    evaluate_model,
    predict_with_model,
    prepare_data,
    split_data,
    train_model,
)

import config as cfg

if __name__ == "__main__":
    data = pd.read_csv(cfg.TRAIN_DATA_PATH)
    data_cleaned = prepare_data(data)
    X_train, X_test, y_train, y_test = split_data(data_cleaned)
    model = train_model(X_train, y_train)
    y_pred = predict_with_model(model, X_test)
    accuracy = evaluate_model(y_test, y_pred)
    print(f"train accuracy: {accuracy}")
