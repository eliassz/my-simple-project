TRAIN_DATA_PATH = "data/train.csv"
INFER_DATA_PATH = "data/infer.csv"
SUBMISSION_PATH = "data/submit.csv"
MODEL_PATH = "models/titanic_model.pkl"
